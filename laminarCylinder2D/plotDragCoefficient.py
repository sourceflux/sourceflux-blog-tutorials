#!/usr/bin/python

import scipy 
import numpy as np
from matplotlib import pyplot as plt

coeffs = np.loadtxt('./postProcessing/forceCoefficients/0/forceCoeffs.dat', skiprows=9)

CdGradient = np.gradient(coeffs[:,2])

minCdGradient = np.min(np.abs(CdGradient))
CdConverged = coeffs[:,2][-1]

plt.ylim([-2.8, 2.8])
plt.xlabel("Simulation time in seconds")
plt.plot(coeffs[:,0], coeffs[:,2], label="Cd, converged = %f" % CdConverged)
plt.plot(coeffs[:,0], CdGradient, label="Cd gradient, minimal = %f" % minCdGradient)
plt.legend(loc="lower left")
plt.savefig('laminarCylinder2D-dragCoeff-diagram.png')
