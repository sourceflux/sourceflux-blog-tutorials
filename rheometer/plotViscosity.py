#!/usr/bin/python

import scipy 
import numpy as np
from matplotlib import pyplot as plt
from matplotlib import rc
import re
from math import pi
from PyFoam.RunDictionary.ParsedParameterFile import ParsedParameterFile 
import sys

rc('text', usetex=True)

def main(): 

    viscosityFileNameBC = "./rheometerCylindricalInletVelocity/viscosity.dat"
    viscosityBC = np.loadtxt(viscosityFileNameBC)

    viscosityFileNameMRF = "./rheometerMRF/viscosity.dat"
    viscosityMRF = np.loadtxt(viscosityFileNameMRF)

    transportProperties = ParsedParameterFile("rheometerMRF/constant/transportProperties")
    prescribedViscosity = float(transportProperties["nu"][-1])

    fig, ax = plt.subplots(1)

    prescribedViscosityVec = np.array(viscosityMRF[:,1])
    prescribedViscosityVec.fill(prescribedViscosity) 

    plt.ylabel("$\eta$")
    plt.xlabel("$N_r$")
    plt.xticks(viscosityMRF[:,0])
    plt.ylim([0.999*np.min(viscosityMRF[:,1]), 1.001*np.max(viscosityMRF[:,1])])
    plt.plot(viscosityMRF[:,0], viscosityMRF[:,1], label="$\eta_{calc, MRF}$", marker='o')
    plt.plot(viscosityBC[:,0], viscosityBC[:,1], label="$\eta_{calc, BC}$", marker='x')
    plt.plot(viscosityMRF[:,0], prescribedViscosityVec, label="$\eta_{exact}$")
    plt.legend(loc="lower right")
    plt.savefig('rheometer-viscosity-convergence.png')

if __name__ == "__main__":
    main()
