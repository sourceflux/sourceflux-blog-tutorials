#!/usr/bin/python

import scipy 
import numpy as np
from matplotlib import pyplot as plt
from matplotlib import rc
import re
from math import pi
from PyFoam.RunDictionary.ParsedParameterFile import ParsedParameterFile 
import sys

def numerical_viscosity(Mvisc, R1, R2, Omega1, Omega2, l):
    return (Mvisc * (R2**2 - R1**2)) / (4 * pi * R1**2 * R2**2 * l * (Omega2 - Omega1)) 

rc('text', usetex=True)

def main(): 

    parameter = '-' 
    
    if (len(sys.argv) > 1):
        parameter += sys.argv[1]
        viscosityFile = open("viscosity.dat", 'a')
    else:
        parameter = ""

    forcesFileName = "./postProcessing/forces/0/forces.dat"

    forcesFileContent = open(forcesFileName).read()
    forcesFileContent = re.sub('[()]', '', forcesFileContent)

    open(forcesFileName, 'w').write(forcesFileContent)

    forces = np.loadtxt(forcesFileName, skiprows=3)

    time = forces[:, 0] 
    moment = 4 * forces[:, 15] 
    momentGrad = np.gradient(moment)

    fig, ax = plt.subplots(1)

    labels = ["$M_{visc, z}$, final = %.4E" % moment[-1], "$\partial_t (M_{visc,z})$, final = %.4E" % momentGrad[-5]]

    plt.yscale("log")
    plt.xlabel("Iteration steps")
    plt.plot(time, moment, color='red')
    plt.plot(time, momentGrad, color='blue')
    plt.legend(labels, loc="lower right")

    plt.savefig('rheometer-viscous-moment-diagram' + parameter + '.png')

    fvOptions = ParsedParameterFile("system/fvOptions")

    angularVelocity = float(fvOptions["MRF1"]["MRFSourceCoeffs"]["omega"]) 

    if (len(sys.argv) > 1):
        viscosityFile.write("%.5E %.5E \n" % (float(sys.argv[1]), numerical_viscosity(moment[-1], 0.1, 0.12, 0, angularVelocity, 0.01)))

if __name__ == "__main__":
    main()
